package com.potato.momskitchen

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity: AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.hide()

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        log_in_button.setOnClickListener{
            loginAccount()
        }

        dont_have_an_account.setOnClickListener{
            Log.d("LoginActivity", "The user have an account::::")
            //Go to Activity signup
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }

    private fun loginAccount() {
        val email = input_email_login.text.toString()
        val password = input_password_login.text.toString()

        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Invalid credentials :/", Toast.LENGTH_SHORT).show()
            return
        }

        Log.d("LoginActivity", "Email is: " + email)
        Log.d("LoginActivity", "Password: $password")

        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this){
                if (it.isSuccessful) {
                    val user = FirebaseAuth.getInstance().currentUser
                    Log.d("Main", "Account login uid = ${it.result!!.user.uid}")
                    return@addOnCompleteListener
                }
                else {
                    Log.d("Main", "Account not connected", it.exception)
                    Toast.makeText(this, "Wow that's not a correct account", Toast.LENGTH_SHORT).show()
                }
            }
            .addOnFailureListener(this){
                Log.d("Main", "Failed to connect user: ${it.message}")
                Toast.makeText(this, "Wow that's not a correct account", Toast.LENGTH_SHORT).show()
            }
    }
}