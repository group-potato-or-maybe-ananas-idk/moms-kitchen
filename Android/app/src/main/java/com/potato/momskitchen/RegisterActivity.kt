package com.potato.momskitchen

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.hide()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sign_up_button.setOnClickListener{
            createAccount()
        }


        already_have_an_account_sign_up.setOnClickListener{
            Log.d("RegisterActivity", "The user doesn't have an account::::")
            //Go to Activity login
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)

        }
    }

    private fun createAccount() {
        val email = input_email_signup.text.toString()
        val password = input_password_signup.text.toString()

        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Invalid credentials :/", Toast.LENGTH_SHORT).show()
            return
        }

        Log.d("RegisterActivity", "Email is: " + email)
        Log.d("RegisterActivity", "Password: $password")

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener{
                if (it.isSuccessful) {
                    val user = FirebaseAuth.getInstance().currentUser
                    Log.d("Main", "Account created uid = ${it.result!!.user.uid}")
                    saveUserToFirebaseDatabase()
                }
                else {
                    Log.d("Main", "Account not created", it.exception)
                    return@addOnCompleteListener
                }
            }
            .addOnFailureListener(this){
                Log.d("Main", "Failed to create user: ${it.message}")
                Toast.makeText(this, "Invalid credentials : ${it.message}", Toast.LENGTH_SHORT).show()

            }
    }

    private fun saveUserToFirebaseDatabase(){
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")

        val user = User("", input_username_signup.text.toString(), uid)

        ref.setValue(user)
            .addOnSuccessListener {
                Log.d("RegisterActivity", "Saved the user to Firebase !")

                val intent = Intent(this, LatestMessagesActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
            .addOnFailureListener{
                Log.d("RegisterActivity", "NOT SAVED TO FIREBASE AHHHH")
            }
    }
}

class User(val profileImageUrl: String, val pseudo: String, val uid: String)