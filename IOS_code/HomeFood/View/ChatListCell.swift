//
//  ChatListCell.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 06/03/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit

class ChatListCell: UITableViewCell{
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    // may be further refactor this.
    func profileImageCornerRadius(){
        self.profileImage.clipsToBounds = true
        self.profileImage.layer.cornerRadius = profileImage.frame.width / 2
    }
    
}

