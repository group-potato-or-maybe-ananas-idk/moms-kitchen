//
//  CustomTextField.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 13/02/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit


class CustomTextField: UITextField{
    let padding: CGFloat
    
    override var intrinsicContentSize: CGSize{
        return CGSize(width: 0, height: 50)
    }
    
    init(padding: CGFloat) {
        self.padding = padding
        super.init(frame: .zero)
        layer.cornerRadius = 5
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: padding, dy: 0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: padding, dy: 0)
    }
}
