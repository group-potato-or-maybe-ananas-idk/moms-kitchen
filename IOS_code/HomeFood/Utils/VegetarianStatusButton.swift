//
//  CustomButton.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 08/03/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit

class VegetarianStatusButton: UIButton{
    
    var isOn  = false
    var colorOn: UIColor?
    var colorOff: UIColor?
    var titleColorOn: UIColor?
    var titleColorOff: UIColor?
    var titleOn:String = ""
    var titleOff:String = ""
    
    required init?(colorOn: UIColor, colorOff: UIColor ,titleOn:String, titleOff: String, titleColorOn: UIColor, titleColorOff: UIColor) {
        super.init(frame: .zero)
        layer.borderWidth = 2.0
        isOn = false
        self.colorOn = colorOn
        self.colorOff = colorOff
        self.titleOn = titleOn
        self.titleOff = titleOff
        self.titleColorOn = titleColorOn
        self.titleColorOff = titleColorOff
        addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
    }
    
    @objc func buttonPressed(){
        activeButton(bool: !isOn)
    }
    
    func activeButton(bool: Bool){
        isOn = bool
        print(bool)
        let color = bool ? colorOn : colorOff
        let titleColor = bool ? titleColorOn: titleColorOff
        let title = bool ? titleOn : titleOff
        backgroundColor = color
        self.setTitleColor(titleColor, for: .normal)
        self.layer.borderColor = titleColor?.cgColor
        self.setTitle(title, for: .normal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Some error")
    }
}
