//
//  CustomNavigationBar.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 10/03/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit

extension UISearchController{
    
    func barSettings(){
        self.searchBar.searchBarStyle = .prominent
        self.searchBar.isTranslucent = false
        self.searchBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.searchBar.barTintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.searchBar.backgroundImage = UIImage()
        
        if let textfield = self.searchBar.value(forKey: "searchField") as? UITextField {
            if let backgroundview = textfield.subviews.first {
                
                // Background color
                backgroundview.backgroundColor = UIColor.white
                backgroundview.layer.cornerRadius = 10;
                backgroundview.clipsToBounds = true;
            }
        }
        
    }
    
}
