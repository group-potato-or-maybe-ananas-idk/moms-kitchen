//
//  CustomButton.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 08/03/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit

class vegetarian: UIButton{
    var isOn  = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.borderWidth = 2.0
        addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
    }
    
    @objc func buttonPressed(){
        activeButton(bool: !isOn)
    }
    
    func activeButton(bool: Bool){
        isOn = bool
        let color = bool ? #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1) : .clear
        let tintColor = bool ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        backgroundColor = color
        self.layer.borderColor = tintColor.cgColor
    
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Some error")
    }
}
