//
//  HomeController.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 17/02/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation

class HomeController: UICollectionViewController{
    
    // MARK:- Properties and Elements
    var locationManger: CLLocationManager!
    var userLocationCoordinate: CLLocationCoordinate2D?
    
    let addButton: UIButton = {
        let button =  UIButton(type: .system)
        button.backgroundColor = UIColor.color(r: 249, g: 152, b: 58)
        button.setTitle("+", for: .normal)
        button.addTarget(self, action: #selector(handlePostButton), for: .touchUpInside)
        button.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 42)
        button.layer.cornerRadius = 32.5
        return button
    }()
    

    
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        navigationSettings()
        setupSideBar()
        tapGesture()
        setupAddItemButton()
    }
    
    // MARK:- objc methods
    
    @objc func handleLogout(){
        let window = UIApplication.shared.keyWindow
        do{
            try Auth.auth().signOut()
            let navigationController = UINavigationController(rootViewController: SignupController())
            window?.rootViewController = navigationController
        }catch let error{
            print("Some error", error.localizedDescription)
        }
    }
    
    let sideViewController = MenuController()
    
    @objc func handleNavigationSideBarButton(){
       
        sideViewController.view.frame = CGRect(x: -300, y: 0, width: 300, height: view.frame.height)
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
             self.sideViewController.view.transform = CGAffineTransform(translationX: 300, y: 0)
        })
        
        let mainWindow = UIApplication.shared.keyWindow
        mainWindow?.addSubview(sideViewController.view)
        addChild(MenuController())
    
    }
    
    
    
    @objc fileprivate func handleTapGesture(){
        dismissMenuController()
    }
    
    @objc func handlePostButton(){
        self.navigationController?.pushViewController(AddListingController(), animated: true)
    }
    

    // MARK:- CollectionView settings
    fileprivate let cellId = "customCell"
    
    fileprivate func setupCollectionView(){
        collectionView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
       
        let layout = collectionViewLayout as! UICollectionViewFlowLayout
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        let customResurantNib = UINib(nibName: "ResturantsCell", bundle: nil)
        collectionView.register(customResurantNib, forCellWithReuseIdentifier: cellId)
        collectionView.register(customSliderHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "headerCell")
    }

    //MARK:- Navigation
    fileprivate func navigationSettings(){
        self.navigationItem.title = "Mom's Kitchen"
        self.navigationController?.navigationBar.prefersLargeTitles = false

        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.black]
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "filter").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleLogout))
        
        let searchController = UISearchController(searchResultsController: nil)
       
        
        navigationItem.searchController = searchController
        searchController.dimsBackgroundDuringPresentation = false
        navigationItem.hidesSearchBarWhenScrolling = false
    }
    
    // MARK:- SetupElementsFunctions / Custom Functions.
    fileprivate func setupSideBar(){
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "side").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleNavigationSideBarButton))
    }
    
    
    fileprivate func setupAddItemButton(){
        self.view.addSubview(addButton)
        addButton.anchor(top: nil, paddingTop: 0, bottom: view.safeAreaLayoutGuide.bottomAnchor, paddingBottom: -5, left: nil, paddingLeft: 0, right: view.rightAnchor, paddingRight: -5, height: 65, width: 65, centerX: nil, centerY: nil)
        
    }
    
    
    fileprivate func tapGesture(){
        let gesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        gesture.cancelsTouchesInView = false
        view.addGestureRecognizer(gesture)
    }
    
    fileprivate func dismissMenuController(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.sideViewController.view.transform = .identity
        })
        
    }
}

// MARK:- CollectionView Delegate methods.

extension HomeController: UICollectionViewDelegateFlowLayout{
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! RestaurantsCell
        cell.timeb.layer.cornerRadius = 8
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ProductController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 262)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    // Here comes the header
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerCell", for: indexPath) as! customSliderHeader
        
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 260)
    }
    
}

