//
//  ChatController.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 26/02/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit
import Firebase


class ChatController: UICollectionViewController{
    
    
    var recipentUid:String?
    var currentUser: UserDetails?
    var chatUid:String?
    
    
    // elements
    
    let messageTextField: CustomTextField = {
        let tf = CustomTextField(padding: 10)
        tf.placeholder = "Enter the message"
        tf.backgroundColor = UIColor.color(r: 240, g: 240, b: 240)
        return tf
    }()
    
    
    let sendButton: UIButton = {
        let bv = UIButton(type: .system)
        bv.setImage(#imageLiteral(resourceName: "send").withRenderingMode(.alwaysOriginal), for: .normal)
        bv.addTarget(self, action: #selector(handleSendButton), for: .touchUpInside)
        return bv
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewSettings()
        setupTextField()
        setupNavigation()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
         self.tabBarController?.tabBar.isHidden = false
    }
    
    fileprivate func collectionViewSettings(){
       collectionView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    
    fileprivate func setupTextField(){
        let stackView = UIStackView(arrangedSubviews: [messageTextField, sendButton])
        stackView.backgroundColor = #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
        stackView.axis = .horizontal
  
        stackView.spacing = 0
        stackView.isLayoutMarginsRelativeArrangement = true
        
        self.view.addSubview(stackView)
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        stackView.anchor(top: nil, paddingTop: 0, bottom: view.safeAreaLayoutGuide.bottomAnchor, paddingBottom: 3, left: view.leftAnchor, paddingLeft: 0, right: view.rightAnchor, paddingRight: 3, height: 50, width: 0, centerX: nil, centerY: nil)
        
        print("added stack view")
    }
    
    
    fileprivate func setupNavigation(){
        self.navigationItem.title = "Chat"
    }
    
    // need to change this logic here and make some stuff. - do it feeling tired now	.
    // change are made now need something which will update the basic property to the user of chat Channels.
    
    func updateMessage(message:String){
        
        let messagesRef = Firestore.firestore().collection("Messages")
        
        if let chatUid  = chatUid{
            messagesRef.document("\(chatUid)")
            let docRef = messagesRef.document("\(chatUid)")
            docRef.updateData(["Message": FieldValue.arrayUnion([message])])
        }else{
            let messageUid = UUID().uuidString
            currentUser?.chatChannels?.append(chatUid ?? "")
            let docRef = messagesRef.document("\(messageUid)")
            docRef.setData(["Message": [message]], merge: true)
        }
        
        
        
        let senderUid = Auth.auth().currentUser?.uid
        
        
        // update the chat reference.
        let userRef = Firestore.firestore().collection("UserDetails")
        let docRef = userRef.document("\(senderUid ?? "")")
        docRef.updateData(["Messages": FieldValue.arrayUnion([chatUid ?? ""])])
        print("Updated the ref")
        
    }
    
    
    @objc func handleSendButton(){
        guard let message = messageTextField.text else { return }
        updateMessage(message: message)
    }
}
