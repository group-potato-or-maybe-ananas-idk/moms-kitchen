//
//  MenuController.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 18/02/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit

class MenuController: UIViewController{
    
    var options:[String] = ["My Listing", "My Account", "Recommendation", "FAQ", "Address", "Near me", "Logout"]

    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return collectionView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        view.addSubview(collectionView)
        collectionView.fillSuperview()
        collectionViewSettings()
    }
    
    fileprivate func collectionViewSettings(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(CustomMenuCell.self, forCellWithReuseIdentifier: "optionsCollectionView")
        collectionView.register(CustomMenuHeaderCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "optionsHeader")
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.scrollDirection = .vertical
    }
    
}

extension MenuController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "optionsCollectionView", for: indexPath) as! CustomMenuCell
        cell.title.text = options[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "optionsHeader", for: indexPath) 
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}


class CustomMenuCell: UICollectionViewCell{
    let title: UILabel = {
        let label = UILabel()
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(title)
        title.anchor(top: nil, paddingTop: 0, bottom: nil, paddingBottom: 0, left: leftAnchor, paddingLeft: 18, right: nil, paddingRight: 0, height: 0, width: 0, centerX: nil, centerY: centerYAnchor)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Some error")
    }
    
}


class CustomMenuHeaderCell: UICollectionViewCell{

    
    let profileImageButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "pp").withRenderingMode(.alwaysOriginal), for: .normal)
        button.clipsToBounds = true
        button.layer.cornerRadius = 75
        return button
    }()
    
    
    fileprivate func setupUserDetails(){
        self.addSubview(profileImageButton)
        profileImageButton.anchor(top: self.topAnchor, paddingTop: 7, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: nil, paddingRight: 0, height: 150, width: 150, centerX: centerXAnchor, centerY: nil)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUserDetails()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Some error")
    }
}
