//
//  compteController.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 17/02/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage

// Needs to make some changes to this project.

class UserProfileController: UIViewController{
    
    var currentUser: UserDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        updateUserProfile()
        fetchDetails()
        setupUserName()
    }
    // Networking code to update user profile information.
    fileprivate func updateProfileImage(image: UIImage, oldImageUrl:String){
        print("Old image which was passed is ", oldImageUrl)
        
        Storage.storage().reference(forURL: oldImageUrl).delete { (error) in
            if let error = error{
                print("Some error deleting the old image", error.localizedDescription)
                return
            }
            // Now the old is deleted we need to upload new image form here
            let newImage = image
            guard let data = newImage.jpegData(compressionQuality: 0.4) else { return }
            let uid = UUID().uuidString
            let uploadRef = Storage.storage().reference(withPath: "profileImages").child(uid)
            uploadRef.putData(data, metadata: nil, completion: { (_, error) in
                if let error = error{
                    print("Some error", error.localizedDescription)
                    return
                }
                
                uploadRef.downloadURL(completion: { (url, error) in
                    if let error = error{
                        print("Some error ", error.localizedDescription)
                        return
                    }
                    guard let uid = Auth.auth().currentUser?.uid else { return };
                    
                    Firestore.firestore().collection("UserDetails").document(uid).updateData(["ProfileImage": url?.absoluteString ?? ""])
                    
                })
            })
        }
        
    }
    
    fileprivate func fetchDetails(){
        let uid = Auth.auth().currentUser?.uid ?? ""
        print("UID IS ", uid)
        let db = Firestore.firestore().collection("UserDetails").document(uid)
        
        db.addSnapshotListener(includeMetadataChanges: true) { (snapShot, error) in
            if let error = error{
                print("Some error", error.localizedDescription)
                return
            }
            guard let imageUrl = snapShot?.data()?["ProfileImage"] as? String else { return }
            guard let userName = snapShot?.data()?["Username"] as? String else { return }
            
            guard let userEmail = snapShot?.data()?["UserEmail"] as? String else  { return }
            guard let uid = snapShot?.data()?["UID"] as? String else { return }
            let imageUrlFinal = URL(string: imageUrl)
            // to get access to the details later on
            let user: UserDetails = UserDetails(userName: userName, userEmail: userEmail, profileImageUrl: imageUrl, uid: uid, chatChannels: nil)
            self.currentUser = user
            
            self.backgroundImageView.sd_setImage(with: imageUrlFinal, completed: nil)
            self.userProfileImageButton.sd_setBackgroundImage(with: imageUrlFinal, for: .normal, completed: nil)
            self.userNameLabel.text = userName
            self.userEmail.text = userEmail
        }
    }
    
    
    fileprivate func updateUserProfile(){
        view.addSubview(backgroundImageView)
        backgroundImageView.anchor(top: view.topAnchor, paddingTop: 0, bottom: nil, paddingBottom: 0, left: view.leftAnchor, paddingLeft: 0, right: view.rightAnchor, paddingRight: 0, height: 320, width: 0, centerX: nil, centerY: nil)
        
        backgroundImageView.addSubview(blurEffectView)
        blurEffectView.fillSuperview()
        view.addSubview(userProfileImageButton)
        
        userProfileImageButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, paddingTop: 10, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: nil, paddingRight: 0, height: 200, width: 200, centerX: view.centerXAnchor, centerY: nil)
    }
    
    fileprivate func setupUserName(){
        blurEffectView.contentView.addSubview(userNameLabel)
        userNameLabel.anchor(top: userProfileImageButton.bottomAnchor, paddingTop: 5, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: nil, paddingRight: 0, height: 0, width: 0, centerX: view.centerXAnchor, centerY: nil)
        blurEffectView.contentView.addSubview(userEmail)
        userEmail.anchor(top: userNameLabel.bottomAnchor, paddingTop: 5, bottom: nil, paddingBottom: 0, left: nil, paddingLeft: 0, right: nil, paddingRight: 0, height: 0, width: 0, centerX: view.centerXAnchor, centerY: nil)
    }
    
    
    // MARK:- @objc methods
    
    @objc func handleProfileImageChange(){
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    // MARK:- Elements
    
    let backgroundImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.image = #imageLiteral(resourceName: "pp")
        return iv
    }()
    
    let blurEffectView: UIVisualEffectView = {
        let effect = UIBlurEffect(style: .light)
        let effectView = UIVisualEffectView(effect: effect)
        return effectView
    }()
    
    let userNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return label
    }()
    
    let userEmail: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return label
    }()
    
    
    let userProfileImageButton: UIButton = {
        let iv = UIButton(type: .system)
        iv.contentMode = .scaleAspectFill
        iv.addTarget(self, action: #selector(handleProfileImageChange), for: .touchUpInside)
        iv.layer.cornerRadius = 100
        iv.clipsToBounds = true
        return iv
    }()
}

extension UserProfileController: UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
        self.backgroundImageView.image = image.withRenderingMode(.alwaysOriginal)
        self.userProfileImageButton.setImage(image.withRenderingMode(.alwaysOriginal), for: .normal)
        let oldImageUrldetails = currentUser?.profileImageUrl ?? ""
        updateProfileImage(image: image, oldImageUrl: oldImageUrldetails)
        dismiss(animated: true, completion: nil)
    }
}
