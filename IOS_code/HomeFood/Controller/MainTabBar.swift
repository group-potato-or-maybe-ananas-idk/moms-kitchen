//
//  HomeController.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 14/02/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit


class MainTabBarController: UITabBarController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabBarItems()
    }
    
    // MARK:- Setup TabBar Items
    fileprivate func setupTabBarItems(){
        self.tabBar.tintColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        
        self.tabBarItem.titleTextAttributes(for: .normal)
        let homeController = HomeController(collectionViewLayout: UICollectionViewFlowLayout())
        let communityController = CommunityController()
        let userProfileController = UserProfileController()
        let chatController = ChatListController()
        let homeNavigationController = UINavigationController(rootViewController: homeController)
        let chatNavigationController = UINavigationController(rootViewController: chatController)
        self.viewControllers = [homeNavigationController, communityController, chatNavigationController, userProfileController]
        homeController.tabBarItem = UITabBarItem(title: "Food", image: #imageLiteral(resourceName: "food1"), selectedImage: nil)
        communityController.tabBarItem = UITabBarItem(title: "Community", image: #imageLiteral(resourceName: "community"), selectedImage: nil)
        userProfileController.tabBarItem = UITabBarItem(title: "User profile", image: #imageLiteral(resourceName: "users"), selectedImage: nil)
        chatController.tabBarItem = UITabBarItem(title: "Chat", image: #imageLiteral(resourceName: "messages"), selectedImage: nil)
    }
    
}

