//
//  OffersViewController.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 17/02/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit

class CommunityController: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
}
