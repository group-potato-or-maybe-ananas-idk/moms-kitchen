//
//  FoodItem.swift
//  HomeFood
//
//  Created by Vinayak Sareen on 18/02/2019.
//  Copyright © 2019 Vinayak Sareen. All rights reserved.
//

import UIKit

struct Fooditem{
    let name:String?
    let imageUrl:String?
    let location:[Any]?
    let likes: Double
    let dislikes: Double
    let locatoinDistance: Double
}
