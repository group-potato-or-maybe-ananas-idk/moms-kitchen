from django.shortcuts import render
from blog.models import Firebase_module,Pyrebase_module
import re


######### page indexing ############
def sign_up(request):
	return render(request,"signUp.html")

def sign_in(request):
	return render(request,"signIn.html")

def password_reset(request):
	return render(request,"passwordReset.html")
####################################


def post_sign_up(request):
	try:
		name=request.POST.get('name')
		email=request.POST.get('email')
		password=request.POST.get('pass')
		image=request.FILES['image']
	except :
			return render(request,"signUp.html",{"messg":'image no load '})

	#### input verification ####

	regexp = r"(^[a-z0-9._-]+@)"

	if(len(password)<6 and len(name)<6 and (re.match(regexp, email) is not None)):
			return render(request,"signUp.html",{"messg":'invalid format for credentials'})
	
	
	#### init ####

	fb = Firebase_module.objects
	
	
	#### register #####	
	try:
		fb.create_user(email,password)
	except:
		message = "email already exist"
		return render(request,"signUp.html",{"messg":message})
	
	# upload picture
	path_picture = fb.upload_image_storage(image)
	# add user's info 
	fb.upload_credentials_database(email,name,path_picture)
	
	return render(request,"resultat.html",{"data":url})

	#except:
	#message = "error while registering"
	#return render(request,"signUp.html",{"messg":message})

