
from django.shortcuts import render,redirect
from blog.models import Firebase_module,Pyrebase_module
import re,json 
import datetime
import requests


fb = Firebase_module()

def create_conversation(request):
	# verify cookie session 
	try:
		token = request.session['token']
		uid = fb.verify_session_token(token)['uid']
	except:
		message ="issue with session, please log in "
		return render(request,"signIn.html",{"data":message})

	uuid_product = request.POST.get('product')

	
	#get 
	uid_vendor = fb.get_uid_vendor(uuid_product)
	
	uuid_conv= Firebase_module.generate_random_uuid()
	fb.create_channel_conversation_for_user(uid,uid_vendor,uuid_conv);
	fb.create_channel_conversation_for_user(uid_vendor,uid,uuid_conv);
	fb.init_conversation("subject",uuid_conv)



	#redirect


def display_all_conversation(request):
	# verify cookie session 
	try:
		token = request.session['token']
		uid = fb.verify_session_token(token)['uid']
	except:
		message ="issue with session, please log in "
		return render(request,"signIn.html",{"data":message})

	data = fb.get_all_conversation(uid)

	for x in data:
		print(x['Username'])
		print(x['uuid_conv'])


def add_message(request):
	# verify cookie session 
	try:
		token = request.session['token']
		uid = fb.verify_session_token(token)['uid']
	except:
		message ="issue with session, please log in "
		return render(request,"signIn.html",{"data":message})
	
	#to_filter
	#message=request.POST.get('message')
	#uuid_conv=request.POST.get('uuid_conv')

	message="test message"
	uuid_conv="f11afdea-67f8-4c2c-a998-d08c74639f20"

	fb.add_message_to_conversation(message,uuid_conv,uid)

def display_a_conversation(request):
	# verify cookie session 
	try:
		token = request.session['token']
		uid = fb.verify_session_token(token)['uid']
	except:
		message ="issue with session, please log in "
		return render(request,"signIn.html",{"data":message})

	#uuid_conv=request.POST.get('uuid_conv')
	uuid_conv="f11afdea-67f8-4c2c-a998-d08c74639f20"

	data = fb.get_a_conversation(uuid_conv)

	return data

def delete_a_conversation(request):
	# verify cookie session 
	try:
		token = request.session['token']
		uid = fb.verify_session_token(token)['uid']
	except:
		message ="issue with session, please log in "
		return render(request,"signIn.html",{"data":message})

	#uuid_conv=request.POST.get('uuid_conv')
	uuid_conv="f11afdea-67f8-4c2c-a998-d08c74639f20"

	fb.delete_a_conversation(uuid_conv)




	