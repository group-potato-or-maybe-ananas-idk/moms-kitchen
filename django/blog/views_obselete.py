from django.shortcuts import render


# dependence problems with auth, use pyrebase instand
#from firebase_admin import auth


# doc API blob
# https://googleapis.github.io/google-cloud-python/latest/storage/blobs.html#google.cloud.storage.blob.Blob.public_url

######## API connection initialisation #########

def init_authentification():
	config = {
		  'apiKey': "AIzaSyC0TACrDN13PHAGiKz29_7OnjjYvcR9-7Y",
    	'authDomain': "homefood-b7713.firebaseapp.com",
    	'databaseURL': "https://homefood-b7713.firebaseio.com",
    	'projectId': "homefood-b7713",
    	'storageBucket': "homefood-b7713.appspot.com",
    	'messagingSenderId': "1004772233083"

	}

	firebase = pyrebase.initialize_app(config)
	authe = firebase.auth()
	return authe


def init_firesotre():
	cred = credentials.Certificate("homefood-b7713-firebase-adminsdk-u8apq-e2983e57b5")
	app = firebase_admin.initialize_app(cred,{
    	'storageBucket': 'homefood-b7713.appspot.com'
	})
	return app

def delete_instance(app):
	firebase_admin.delete_app(app)	
	

######### page indexing ############
def signUp(request):
	return render(request,"signUp.html")

def signIn(request):
	return render(request,"signIn.html")

def passwordReset(request):
	return render(request,"passwordReset.html")
####################################

def postsignup(request):
	try:
		name=request.POST.get('name')
		email=request.POST.get('email')
		password=request.POST.get('pass')
		image=request.FILES['image']
	except :
			return render(request,"signUp.html",{"messg":'image no load '})

	#### input verification ####
	if(len(password)<6 and len(name)<6 and len(email)<5 ):
			return render(request,"signUp.html",{"messg":''})
	
	#### init ####
	try:
		cred = credentials.Certificate("homefood-b7713-firebase-adminsdk-u8apq-e2983e57b5.json")
		app = firebase_admin.initialize_app(cred)
		db=firestore.client()
		bucket=storage.bucket('homefood-b7713.appspot.com')
	except:
		message = "error during connection initialisation"
		return render(request,"signUp.html",{"messg":message})
	
	#### register #####	
	try:
		user = auth.create_user( email=email,password=password)
	except:
		message = "email already exist"
		return render(request,"signUp.html",{"messg":message})

	uid=user.uid		
	# upload picture
	url= uploadImage(image,uid,bucket)
	# add user's info 
	doc_ref = db.collection(u'UsersDetails').document(uid)
	doc_ref.set({
   			u'UserEmail': email,
 	   		u'uid': uid,
	   		u'Username': name,
	   		u'VerificationStatus' : 0,
	   		u'profileImage' : 'https://firebasestorage.googleapis.com/v0'+url
		})

	
	return render(request,"resultat.html",{"data":url})

	#except:
	#message = "error while registering"
	#return render(request,"signUp.html",{"messg":message})


def postSignIn(request):
	email=request.POST.get('email')
	passw = request.POST.get("pass")

	#### init #####
	try:
		authe=init_authentification()

	except:
		message = "error during connection initialisation"
		return render(request,"signIn.html",{"messg":message})		

	#### credentials verification #####	
	try:
		user = authe.sign_in_with_email_and_password(email,passw)
		return render(request,"resultat.html",{"data":email})
	except:
		message="invalid credentials"
		return render(request,"signIn.html",{"messg":message})


def uploadImage(image,uid,bucket):
	blob = bucket.blob('profileImages/'+uid+'.png')
	blob.upload_from_file(image)
	return blob.path 

def postPasswordReset(request):
	email=request.POST.get('email')
	authe=init_authentification()
	authe.send_password_reset_email(email)

	try:
		message="email sent"
	except:
		message="error: please retry "
	return render(request,"signIn.html",{"messg":message})