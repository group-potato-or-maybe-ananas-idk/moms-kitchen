from django.urls import path
from django.conf.urls import url,re_path
from . import views

urlpatterns = [
    re_path('signUp', views.sign_up,name='sign_up'),
    re_path('^postSignUp', views.post_sign_up,name='post_sign_up'),
    re_path('postSignIn/<int:redirection>/',views.post_sign_in,name='post_sign_in'),
   	re_path('signIn/<redirection>',views.sign_in,name='sign_in'),
   	re_path('^passwordReset',views.password_reset,name='password_reset'),
   	re_path('^postPasswordReset',views.post_password_reset,name='post_password_reset'),
   	re_path('^logOut',views.log_out,name='log_out'),
	re_path('^welcome',views.welcome,name='welcome'),
	re_path('^foodForm',views.food_form,name='food_form'),
	re_path('^postFoodForm',views.post_food_form,name='post_food_form'),
  re_path('^foodListing',views.display_food_list,name='display_food_list'),
  re_path('^convCreation',views.create_conversation,name='create_conversation'),
  re_path('^convDisplayAll',views.display_all_conversation,name='display_all_conversation'),
  re_path('^convAddMessage',views.add_message,name='add_message'),
  re_path('^convDisplayMessage',views.display_a_conversation,name='display_a_conversation'),
   re_path('^get_data',views.get_data,name='get_data'),
   re_path('^display_data',views.display_data,name='display_data'),

  
]