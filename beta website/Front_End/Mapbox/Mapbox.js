//sk.eyJ1IjoibW9tc2tpdGNoZW4iLCJhIjoiY2p0b202NmM1MW42cTQ5cWpzM2hkdnN2bSJ9.KAemGzI1rSf--_2NmMdx6g
mapboxgl.accessToken = 'pk.eyJ1IjoibW9tc2tpdGNoZW4iLCJhIjoiY2p0aGJlMnZuMDg0dTQzbHFtbWV4aWpwOCJ9.veCpV_A9CVP2LTlRD0beeA';
const map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/momskitchen/cjthcduae00li1ft7dnkilepj',
  center: [2.35, 48.85],
  zoom: 11
});
/* Js function to go to Coventry
document.getElementById('Coventry').addEventListener('click', function () {
    // Fly to a random location by offsetting the point -74.50, 40
    // by up to 5 degrees.
    map.flyTo({
        center: [-1.511, 52.408],
        zoom: 14
    });
});
*/
// Add zoom and rotation controls to the map.
map.addControl(new mapboxgl.NavigationControl());   
// Add geolocate control to the map.
map.addControl(new mapboxgl.GeolocateControl({
    positionOptions: {
        enableHighAccuracy: true
    },
    trackUserLocation: true
}));