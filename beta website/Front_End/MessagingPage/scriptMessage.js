window.onload = function() {
    getContact();
}

function keyCode(event) {
    var x = event.keyCode;
    if (x == 13) {
        document.getElementById("btn").click();
    }
  }

function addSend() {
    var text = document.querySelector("#message").value;
    document.querySelector("#message").value = "";
    if (text != "")
    {       
        var msg = document.querySelector(".msg-page");
        var newChat = document.createElement("div");
        newChat.className = "sent-chats";
        var newMsg = document.createElement("div");
        newMsg.className = "sent-msg";
        var newText = document.createElement("p");
        newText.innerText = text;
        var newTime = document.createElement("span");
        newTime.className = "Time";
        var t = new Date();
        newTime.textContent = t.getDate() + "/" + (t.getMonth()+1) + "/" + t.getFullYear() + " " + t.getHours() + ":" + ((t.getMinutes()<10?'0':'') + t.getMinutes());
        newMsg.appendChild(newText);
        newMsg.appendChild(newTime);
        newChat.appendChild(newMsg);
        msg.appendChild(newChat);
        addReceived();
        document.querySelector('.msg-page').lastChild.scrollIntoView(false);
    }

}

function addReceived() {
	var msg = document.querySelector(".msg-page");
	var newChat = document.createElement("div");
	newChat.className = "received-chats";
	var newMsg = document.createElement("div");
	newMsg.className = "received-msg";
	var newText = document.createElement("p");
    newText.textContent = "Hi !";
    var newTime = document.createElement("span");
    newTime.className = "Time";
    var t = new Date();
    newTime.textContent = t.getDate() + "/" + (t.getMonth()+1) + "/" + t.getFullYear() + " " + t.getHours() + ":" + ((t.getMinutes()<10?'0':'') + t.getMinutes())   ;
    newMsg.appendChild(newText);
    newMsg.appendChild(newTime);
    newChat.appendChild(newMsg);
    msg.appendChild(newChat);
}

function getContact() {
    console.log("ok");
    var header = document.querySelector(".msg-header");
    var div = document.createElement("div");
    div.className = "active";
    var contactName = document.createElement("h4");
    contactName.textContent = "Contact";
    div.appendChild(contactName);
    header.appendChild(div);
}